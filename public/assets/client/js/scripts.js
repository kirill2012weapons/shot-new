window.onload = function () {

  document.getElementById('submit_url').onkeypress = function (e) {
    if (e.keyCode == 13)
      return false;
  }
  document.getElementById('input_url').onkeypress = function (e) {
    if (submit_disabled)
      if (e.keyCode == 13)
        return false;
  }
  document.getElementById('submit_url').onclick = function () {
    if (submit_disabled) return false;
    //if (input_url_recent == document.getElementById('input_url').value) return false;
    check_url();
    return false;
  }
  document.getElementById('input_link').onclick = function () {
    document.getElementById('input_link').select();
  }
}

var request;
var response;

var base_domain = 'www.siteshot.net';
var url_base = 'http://www.siteshot.net';
var site_url;
var json_url;
var image_url;

var coming_soon = false;
var request_iterations;
var max_request_iterations = 65;
var request_period_ms = 1000;
var submit_disabled = false;
var input_url_recent;

function check_url() {
  if (!document.getElementById('input_url').value.match(/[\w]+\.[\w]{2,4}/i))
    return;
  else {
    request_iterations = 0;
    site_url = document.getElementById('input_url').value;
    input_url_recent = site_url;
    get_url();
    disable_submit();
  }
}

function get_url() {
  request_iterations++;
  hide_screenshot();
  show_progress();
  document.getElementById('screenshot_image').src = '';
  json_url = '/getimage.php?type=json&url=' + site_url + '&r=' + Math.random() + '&now';
  image_url = '/getimage.php?type=png&w=960&h=600&url=' + site_url;
  set_r_id();
  request_submit(json_url);
}

function request_done(data) {
  eval('response = ' + data);
  coming_soon = response.coming_soon || coming_soon;
  if (request_iterations > max_request_iterations) {
    document.getElementById('screenshot_image').src = url_base + image_url;
    document.getElementById('input_link').value = '<img src="' + url_base + image_url + '" />';
    hide_progress();
    show_screenshot();
    enable_submit();
  } else {
    if (response.is_exists) {
      document.getElementById('screenshot_image').src = url_base + image_url;
      document.getElementById('input_link').value = '<img src="' + url_base + image_url + '" />';
      hide_progress();
      show_screenshot();
      enable_submit();
    } else {
//      if (coming_soon) {
      setTimeout(get_url, request_period_ms);
//      }
//      else {
//        document.getElementById('screenshot_image').src = response.url;
//        document.getElementById('input_link').value = url_base + image_url;
//        show_screenshot();
//        hide_progress();
//        enable_submit();
//      }
    }
  }
}

function show_progress() {
  document.getElementById('progress_container').style.display = 'block';
}

function hide_progress() {
  document.getElementById('progress_container').style.display = 'none';
}

function show_screenshot() {
  document.getElementById('screenshot_container').style.display = 'block';
  document.getElementById('screenshot_image').style.display = 'block';
  document.getElementById('input_link_container').style.display = 'block';
}

function hide_screenshot() {
  document.getElementById('screenshot_container').style.display = 'none';
  document.getElementById('screenshot_image').style.display = 'none';
  document.getElementById('input_link_container').style.display = 'none';
}

function request_error(statusText) {
  alert(statusText);
}

function request_submit(url) {
  request = new XMLHttpRequest();
  request.onreadystatechange = request_status_change;
  request.open("GET", url, true);
  request.send(null);
}

function request_status_change() {
  if (request.readyState == 4) {
    if (request.status == 200) {
      request_done(request.responseText);
    } else {
      request_error(request.statusText);
    }
  }
}

function set_r_id() {
  var st = site_url;
  var r_id = 100000 + st.length;
  for (var i in st) {
    if (st[i].charCodeAt(0) < 128) {
      r_id += st[i].charCodeAt(0);
    }
  }
  var temp = r_id;
  setcookie('r_id', r_id, base_domain);
}

function setcookie(name, value, domain, expires, path, secure) {
  document.cookie = name + "=" + escape(value) +
    ((expires) ? "; expires=" + expires : "") +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    ((secure) ? "; secure" : "");
}

function disable_submit() {
  submit_disabled = true;
}

function enable_submit() {
  setTimeout(enable_submit_timeout, 1000);
}

function enable_submit_timeout() {
  submit_disabled = false;
}