<?php


namespace App\Service\ScreenShot;


use Symfony\Bundle\MakerBundle\Validator;

class ScreenShot
{

  private $shotSaver;
  private $fileManager;

  public function __construct(
    ScreenShotSaver $shotSaver,
    ScreenShotFileManager $fileManager
  )
  {
    $this->fileManager = $fileManager;
    $this->shotSaver = $shotSaver;
  }

  public function gettingPath(string $url)
  {
    $hash = md5($url);

    $path =
      substr($hash, 0, 1) . DIRECTORY_SEPARATOR .
      substr($hash, 1, 1) . DIRECTORY_SEPARATOR .
      substr($hash, 2, 1) . DIRECTORY_SEPARATOR .
      substr($hash, 3) . DIRECTORY_SEPARATOR;

    return $path;
  }

  public function createFullSizeImageDir($dir)
  {
    $defaultDir = $dir . 'fullsize';

    return $this->fileManager->createPath($defaultDir);
  }

}