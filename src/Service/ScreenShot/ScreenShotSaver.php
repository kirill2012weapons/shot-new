<?php


namespace App\Service\ScreenShot;


use App\Event\ImagesEvents\ImageCreatedEvent;
use App\Event\ImagesEvents\ImageExceptionErrorEvent;
use App\Event\ImagesEvents\ImageFailedEvent;
use Psr\Log\LoggerInterface;
use Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ScreenShotSaver
{

  private $browserShot;
  private $bag;
  private $filesystem;
  private $dispatcher;
  private $errorImagesLogger;

  public function __construct(
    \Spatie\Browsershot\Browsershot $browsershot,
    ParameterBagInterface $bag,
    Filesystem $filesystem,
    EventDispatcherInterface $dispatcher,
    LoggerInterface $errorImagesLogger
  )
  {
    $this->browserShot = $browsershot;
    $this->bag = $bag;
    $this->filesystem = $filesystem;
    $this->dispatcher = $dispatcher;
    $this->errorImagesLogger = $errorImagesLogger;
  }

  public function saveUrlImage(string $url, string $filePath = '')
  {

    try {

      $this->browserShot->setUrl($url);
      $this->browserShot->save($this->bag->get('screenshot_directory') . rtrim($filePath) . DIRECTORY_SEPARATOR . 'siteshot.png');

    } catch (CouldNotTakeBrowsershot | ProcessFailedException $e) {

      $eventException = new ImageExceptionErrorEvent( $e->getMessage() );
      $this->dispatcher->dispatch($eventException, ImageExceptionErrorEvent::NAME);

    }

    if (file_exists($this->bag->get('screenshot_directory') . rtrim($filePath) . DIRECTORY_SEPARATOR . 'siteshot.png')) {

      $eventSuccess = new ImageCreatedEvent('File created - ' . $this->bag->get('screenshot_directory') . rtrim($filePath) . DIRECTORY_SEPARATOR . 'siteshot.png - ' . $url);
      $this->dispatcher->dispatch($eventSuccess, ImageCreatedEvent::NAME);
      return true;

    } else {

      $eventFailed = new ImageFailedEvent('File not created - ' . $this->bag->get('screenshot_directory') . rtrim($filePath) . DIRECTORY_SEPARATOR . 'siteshot.png - ' . $url);
      $this->dispatcher->dispatch($eventFailed, ImageFailedEvent::NAME);
      return false;

    }

  }

}