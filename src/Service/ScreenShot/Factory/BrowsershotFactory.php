<?php


namespace App\Service\ScreenShot\Factory;


use Spatie\Browsershot\Browsershot;

class BrowsershotFactory
{

  /**
   * @param \Symfony\Component\DependencyInjection\ParameterBag\ContainerBag $parameterBag
   * @return Browsershot
   */
  public static function getBrowsershot(\Symfony\Component\DependencyInjection\ParameterBag\ContainerBag $parameterBag)
  {

    $nodePath = $parameterBag->get('node_path');
    $defaultImageHeight = $parameterBag->get('default_image_height');
    $defaultImageWidth = $parameterBag->get('default_image_width');

    $browserShot = new Browsershot();

    if (!empty($nodePath)) $browserShot->setNodeBinary($nodePath);
    $browserShot->windowSize($defaultImageWidth, $defaultImageHeight);

    return $browserShot;
  }

}