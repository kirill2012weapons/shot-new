<?php


namespace App\Service\ScreenShot;


use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ScreenShotFileManager
{

  private $filesystem;
  private $bag;

  public function __construct(
    Filesystem $filesystem,
    ParameterBagInterface $bag
  )
  {
    $this->filesystem = $filesystem;
    $this->bag = $bag;
  }

  public function createPath($path)
  {
    $absoluteImagesPath = $this->bag->get('screenshot_directory');
    if (!$this->filesystem->exists($absoluteImagesPath)) $this->filesystem->mkdir($absoluteImagesPath);

    if (!$this->filesystem->exists($absoluteImagesPath . $path)) $this->filesystem->mkdir($absoluteImagesPath .$path);
    return $path;
  }

}