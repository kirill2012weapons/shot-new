<?php


namespace App\Event\ImagesEvents;


use Symfony\Contracts\EventDispatcher\Event;

class ImageFailedEvent extends Event
{
  public const NAME = 'images.failed';

  private $message;

  public function __construct($message = '')
  {
    $this->message = $message;
  }

  public function getMessage()
  {
    return $this->message;
  }
}