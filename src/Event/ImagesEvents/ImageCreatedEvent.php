<?php


namespace App\Event\ImagesEvents;


use Symfony\Contracts\EventDispatcher\Event;

class ImageCreatedEvent extends Event
{
  public const NAME = 'images.created';

  private $message;

  public function __construct($message = '')
  {
    $this->message = $message;
  }

  public function getMessage()
  {
    return $this->message;
  }
}