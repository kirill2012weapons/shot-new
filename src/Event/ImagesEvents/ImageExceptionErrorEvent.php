<?php


namespace App\Event\ImagesEvents;


use Symfony\Contracts\EventDispatcher\Event;

class ImageExceptionErrorEvent extends Event
{
  public const NAME = 'images.exception-error';

  private $message;

  public function __construct($message = '')
  {
    $this->message = $message;
  }

  public function getMessage()
  {
    return $this->message;
  }
}