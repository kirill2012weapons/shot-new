<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class QueueProc
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="queue_processed",indexes={
 *   @ORM\Index(name="idx_url_p", columns={"url"}, flags={"fulltext"}),
 * })
 */
class QueueProc
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\OneToOne(targetEntity="Url")
   * @ORM\JoinColumn(name="url_id", referencedColumnName="id")
   */
  private $urlEntity;

  /**
   * @ORM\Column(type="text", length=500)
   */
  private $url;

  /**
   * @ORM\Column(type="string")
   */
  private $imagePath;

  /**
   * @ORM\Column(type="smallint")
   */
  private $status;

  /**
   * @ORM\Column(type="smallint")
   */
  private $isLoad;

}
