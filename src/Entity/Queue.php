<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Queue
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="queue",indexes={
 *   @ORM\Index(name="idx_url_q", columns={"url"}, flags={"fulltext"}),
 *   @ORM\Index(name="idx_priority_q", columns={"priority"}),
 *   @ORM\Index(name="idx_display_q", columns={"display"})
 * })
 */
class Queue
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\OneToOne(targetEntity="Url")
   * @ORM\JoinColumn(name="url_id", referencedColumnName="id")
   */
  private $urlEntity;

  /**
   * @ORM\Column(type="text", length=500)
   */
  private $url;

  /**
   * @ORM\Column(type="integer")
   */
  private $priority;

  /**
   * @ORM\Column(type="string")
   */
  private $imagePath;

  /**
   * @ORM\Column(type="smallint")
   */
  private $status;

  /**
   * @ORM\Column(type="datetime")
   */
  private $statusTime;

  /**
   * @ORM\Column(type="smallint")
   */
  private $display;

}
