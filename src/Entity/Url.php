<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Url
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="url",indexes={
 *   @ORM\Index(name="idx_url", columns={"url"}, flags={"fulltext"}),
 *   @ORM\Index(name="idx_status", columns={"status"}),
 *   @ORM\Index(name="idx_refreshed_at", columns={"refreshed_at"})
 * })
 */
class Url
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="text", length=500)
   */
  private $url;

  /**
   * @ORM\Column(type="string", length=250)
   */
  private $title;

  /**
   * @ORM\Column(type="datetime")
   */
  private $refreshPeriod;

  /**
   * @ORM\Column(type="datetime")
   */
  private $refreshedAt;

  /**
   * @ORM\Column(type="smallint")
   */
  private $status;

  /**
   * @ORM\Column(type="smallint")
   */
  private $isAdult;

}