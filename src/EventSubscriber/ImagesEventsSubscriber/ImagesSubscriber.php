<?php


namespace App\EventSubscriber\ImagesEventsSubscriber;


use App\Event\ImagesEvents\ImageCreatedEvent;
use App\Event\ImagesEvents\ImageExceptionErrorEvent;
use App\Event\ImagesEvents\ImageFailedEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ImagesSubscriber implements EventSubscriberInterface
{

  private $createdImagesLogger;
  private $errorImagesLogger;
  private $exceptionImagesLogger;

  /**
   * ImagesSubscriber constructor.
   * @param LoggerInterface $createdImagesLogger
   * @param LoggerInterface $errorImagesLogger
   * @param LoggerInterface $exceptionImagesLogger
   */
  public function __construct(
    LoggerInterface $createdImagesLogger,
    LoggerInterface $errorImagesLogger,
    LoggerInterface $exceptionImagesLogger
  )
  {
    $this->createdImagesLogger = $createdImagesLogger;
    $this->errorImagesLogger = $errorImagesLogger;
    $this->exceptionImagesLogger = $exceptionImagesLogger;
  }

  public static function getSubscribedEvents()
  {
    return [
      ImageFailedEvent::NAME => 'onImageFailed',
      ImageExceptionErrorEvent::NAME => 'onImageExceptionError',
      ImageCreatedEvent::NAME => 'onImageCreated',
    ];
  }

  public function onImageFailed(ImageFailedEvent $event)
  {
    $this->errorImagesLogger->error( $event->getMessage() );
  }

  public function onImageExceptionError(ImageExceptionErrorEvent $event)
  {
    $this->exceptionImagesLogger->error( $event->getMessage() );
  }

  public function onImageCreated(ImageCreatedEvent $event)
  {
    $this->createdImagesLogger->info( $event->getMessage() );
  }

}