<?php


namespace App\Controller\Admin;


use App\Service\ScreenShot\ScreenShot;
use App\Service\ScreenShot\ScreenShotSaver;
use Psr\Log\LoggerInterface;
use Spatie\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ScreenShotController extends AbstractController
{

  /**
   * @param ScreenShotSaver $saver
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function index(
    ScreenShot $shot,
    ScreenShotSaver $shotSaver
  )
  {

    $path = $shot->gettingPath('https://www.geeksforgeeks.org/php-md5-sha1-hash-functions/');
    $isCreatedPath = $shot->createFullSizeImageDir($path);
    $shotSaver->saveUrlImage('https://www.geeksforgeeks.org/php-md5-sha1-hash-functions/', $isCreatedPath);

    return $this->render(
      'admin/screenshot/index.html.twig'
    );
  }

}