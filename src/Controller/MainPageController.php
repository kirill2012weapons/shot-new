<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MainPageController extends AbstractController
{

  public function index()
  {
    return $this->render(
      'client/main-page/index.html.twig'
    );
  }

}