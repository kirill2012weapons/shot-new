<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191213155626 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE queue (id INT AUTO_INCREMENT NOT NULL, url_id INT DEFAULT NULL, url TEXT NOT NULL, priority INT NOT NULL, image_path VARCHAR(255) NOT NULL, status SMALLINT NOT NULL, status_time DATETIME NOT NULL, display SMALLINT NOT NULL, UNIQUE INDEX UNIQ_7FFD7F6381CFDAE7 (url_id), FULLTEXT INDEX idx_url_q (url), INDEX idx_priority_q (priority), INDEX idx_display_q (display), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE queue_processed (id INT AUTO_INCREMENT NOT NULL, url_id INT DEFAULT NULL, url TEXT NOT NULL, image_path VARCHAR(255) NOT NULL, status SMALLINT NOT NULL, is_load SMALLINT NOT NULL, UNIQUE INDEX UNIQ_1E3A8B881CFDAE7 (url_id), FULLTEXT INDEX idx_url_p (url), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE url (id INT AUTO_INCREMENT NOT NULL, url TEXT NOT NULL, title VARCHAR(250) NOT NULL, refresh_period DATETIME NOT NULL, refreshed_at DATETIME NOT NULL, status SMALLINT NOT NULL, is_adult SMALLINT NOT NULL, FULLTEXT INDEX idx_url (url), INDEX idx_status (status), INDEX idx_refreshed_at (refreshed_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE queue ADD CONSTRAINT FK_7FFD7F6381CFDAE7 FOREIGN KEY (url_id) REFERENCES url (id)');
        $this->addSql('ALTER TABLE queue_processed ADD CONSTRAINT FK_1E3A8B881CFDAE7 FOREIGN KEY (url_id) REFERENCES url (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE queue DROP FOREIGN KEY FK_7FFD7F6381CFDAE7');
        $this->addSql('ALTER TABLE queue_processed DROP FOREIGN KEY FK_1E3A8B881CFDAE7');
        $this->addSql('DROP TABLE queue');
        $this->addSql('DROP TABLE queue_processed');
        $this->addSql('DROP TABLE url');
    }
}
