<?php

namespace App\Command\Admin;


use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{

  /**
   * @var EntityManager
   */
  private $em = null;

  /**
   * @var UserPasswordEncoderInterface
   */
  private $pe = null;

  protected static $defaultName = 'app:create-user-admin';

  private $adminEmail = 'admin@shot.com';
  private $adminPassword = 'shot';

  public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
  {
    $this->em = $em;
    $this->pe = $passwordEncoder;
    parent::__construct();
  }

  protected function configure()
  {
    $this
      ->setDescription('Create user Administrator :)')
      ->setHelp(
        'Just write this command and you created superuser with ' .
        PHP_EOL .
        $this->adminEmail .
        ' - login' .
        PHP_EOL .
        $this->adminPassword .
        ' - password'
      )
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {

    $output->writeln([
      'Create new User with credentials - ' . $this->adminEmail . ':' . $this->adminPassword,
      '============',
    ]);

    $user = new User();
    $user->setEmail( $this->adminEmail );
    $user->setPassword( $this->pe->encodePassword( $user, $this->adminPassword ) );
    $user->setRoles(['ROLE_SUPER_ADMIN']);
    $user->setName('SiteShot');

    try {

      $userInBD = $this->em->getRepository(User::class)
        ->getUserByEmail( $this->adminEmail )
      ;

      if (!empty($userInBD)) {
        $output->writeln('Cannot create new User - User is exist in database');
        return 0;
      }

      $this->em->persist($user);
      $this->em->flush();

      $output->writeln('New user was created.');

    } catch (ORMException | ORMInvalidArgumentException | \Doctrine\ORM\OptimisticLockException $e) {

      $output->writeln('Cannot create new User - ' . $e->getMessage());

    }

    return 0;

  }

}